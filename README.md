# Auto Login


## Dependency

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the request.

```
pip install request
```

## Usage

```
Enter username and password in the script.
Run the script at startup as a background process.
In Linux add the entry in .profile.
In Windows run it via task scheduler.
```

## Contributing
Pull requests are welcome. For significant changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
MIT
